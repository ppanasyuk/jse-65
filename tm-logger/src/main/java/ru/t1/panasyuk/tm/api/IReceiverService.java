package ru.t1.panasyuk.tm.api;

import org.jetbrains.annotations.NotNull;

import javax.jms.MessageListener;

public interface IReceiverService {

    void receive(@NotNull MessageListener listener);

}