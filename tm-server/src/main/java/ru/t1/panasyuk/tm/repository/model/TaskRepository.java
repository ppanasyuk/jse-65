package ru.t1.panasyuk.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.panasyuk.tm.model.Task;

import java.util.List;

@Repository
@Scope("prototype")
public interface TaskRepository extends AbstractUserOwnedRepository<Task> {

    @NotNull
    @Query("FROM Task m WHERE m.user.id = :userId AND m.project.id = :projectId ORDER BY m.created DESC")
    List<Task> findByProjectId(@NotNull @Param("userId") String userId, @Nullable @Param("projectId") String projectId);

    @Modifying
    @Query("DELETE FROM Task m WHERE m.user.id = :userId AND m.project.id = :projectId")
    void deleteByProjectId(@NotNull @Param("userId") String userId, @Nullable @Param("projectId") String projectId);

    @Modifying
    @Query("DELETE FROM Task m WHERE m.user.id = :userId")
    void deleteByUserId(@NotNull @Param("userId") String userId);

    @NotNull
    @Query("FROM Task m WHERE m.user.id = :userId ORDER BY m.created DESC")
    List<Task> findByIndex(@NotNull @Param("userId") String userId, @NotNull Pageable pageable);

    @NotNull
    @Query("FROM Task m WHERE m.user.id = :userId ORDER BY m.created DESC")
    List<Task> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @NotNull
    @Query("FROM Task m WHERE m.user.id = :userId ORDER BY m.name")
    List<Task> findAllOrderByName(@NotNull @Param("userId") String userId);

    @NotNull
    @Query("FROM Task m WHERE m.user.id = :userId ORDER BY m.status")
    List<Task> findAllOrderByStatus(@NotNull @Param("userId") String userId);

    @Nullable
    @Query("FROM Task m WHERE m.user.id = :userId AND m.id = :id ORDER BY m.created DESC")
    Task findById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @NotNull
    @Query("FROM Task m WHERE m.user.id = :userId")
    List<Task> findByUserId(@NotNull @Param("userId") String userId, @NotNull Sort sort);

    @Query("SELECT COUNT(m) FROM Task m WHERE m.user.id = :userId")
    long countByUserId(@NotNull @Param("userId") String userId);

}