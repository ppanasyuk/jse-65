package ru.t1.panasyuk.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.panasyuk.tm.dto.model.SessionDTO;

import java.util.List;

@Repository
@Scope("prototype")
public interface SessionDtoRepository extends AbstractUserOwnedDtoRepository<SessionDTO> {

    @Nullable
    @Query("FROM SessionDTO m WHERE m.userId = :userId ORDER BY m.created DESC")
    List<SessionDTO> findByIndex(@NotNull @Param("userId") String userId, @NotNull Pageable pageable);

    long countByUserId(@NotNull String userId);

    void deleteByUserId(@NotNull String userId);

    @Nullable
    SessionDTO findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    List<SessionDTO> findByUserId(@NotNull String userId, @NotNull Sort sort);

}