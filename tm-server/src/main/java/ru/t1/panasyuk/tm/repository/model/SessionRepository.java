package ru.t1.panasyuk.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.panasyuk.tm.model.Session;

import java.util.List;

@Repository
@Scope("prototype")
public interface SessionRepository extends AbstractUserOwnedRepository<Session> {

    @Modifying
    @Query("DELETE FROM Session m WHERE m.user.id = :userId")
    void deleteByUserId(@NotNull @Param("userId") String userId);

    @NotNull
    @Query("FROM Session m WHERE m.user.id = :userId ORDER BY m.created DESC")
    List<Session> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @Nullable
    @Query("FROM Session m WHERE m.user.id = :userId AND m.id = :id ORDER BY m.created DESC")
    Session findById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable
    @Query("FROM Session m WHERE m.user.id = :userId ORDER BY m.created DESC")
    List<Session> findByIndex(@NotNull @Param("userId") String userId, @NotNull Pageable pageable);

    @Query("SELECT COUNT(m) FROM Session m WHERE m.user.id = :userId")
    long countByUserId(@NotNull @Param("userId") String userId);

}