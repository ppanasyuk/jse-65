package ru.panasyuk.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.panasyuk.tm.api.service.ITaskService;
import ru.panasyuk.tm.model.Task;
import ru.panasyuk.tm.repository.TaskRepository;

import java.util.List;

@Service
public class TaskService implements ITaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Nullable
    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @NotNull
    @Override
    @Transactional
    public Task save(@NotNull final Task task) {
        return taskRepository.save(task);
    }

    @Override
    @Transactional
    public void saveAll(@NotNull final List<Task> tasks) {
        taskRepository.saveAll(tasks);
    }

    @Nullable
    @Override
    public Task findById(@NotNull final String id) {
        return taskRepository.findById(id).orElse(null);
    }

    @Override
    public boolean existById(@NotNull final String id) {
        return taskRepository.existsById(id);
    }

    @Override
    public long count() {
        return taskRepository.count();
    }

    @Transactional
    public void deleteById(@NotNull final String id) {
        taskRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void delete(@NotNull final Task task) {
        taskRepository.delete(task);
    }

    @Override
    @Transactional
    public void clear(@NotNull final List<Task> tasks) {
        taskRepository.deleteAll(tasks);
    }

    @Override
    public void clear() {
        taskRepository.deleteAll();
    }

}