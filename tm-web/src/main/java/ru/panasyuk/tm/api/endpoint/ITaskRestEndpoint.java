package ru.panasyuk.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.RequestBody;
import ru.panasyuk.tm.model.Task;

import java.util.List;

public interface ITaskRestEndpoint {

    @Nullable
    List<Task> findAll();

    @NotNull
    Task save(@NotNull Task task);

    void saveAll(@NotNull List<Task> tasks);

    @Nullable
    Task findById(@NotNull String id);

    boolean existById(@NotNull String id);

    long count();

    void deleteById(@NotNull String id);

    void delete(@NotNull Task task);

    void clear(@NotNull List<Task> tasks);

    void clear();

}