package ru.panasyuk.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.RequestBody;
import ru.panasyuk.tm.model.Project;

import java.util.List;

public interface IProjectRestEndpoint {

    @Nullable
    List<Project> findAll();

    @NotNull
    Project save(@NotNull Project project);

    public void saveAll(@NotNull List<Project> projects);

    @Nullable
    Project findById(@NotNull String id);

    boolean existById(@NotNull String id);

    long count();

    void deleteById(@NotNull String id);

    void delete(@NotNull Project project);

    void clear(@NotNull List<Project> projects);

    void clear();

}